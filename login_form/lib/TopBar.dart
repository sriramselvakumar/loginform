import 'package:flutter/material.dart';
import './SizeManager.dart';

class TopBar extends StatelessWidget{
  Widget build(BuildContext context) {

    SizeManager sizeManager = new SizeManager(context);

    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: sizeManager.scaledHeight(3)),
      height: sizeManager.scaledHeight(10),
      child: Align(
        alignment: Alignment.center,
        child: Container(
            child:  Image(
                height: sizeManager.scaledHeight(7),
                width: sizeManager.scaledWidth(7),
                image: AssetImage("assets/Logo.png")
            )
        )
      )
    );
  }
}