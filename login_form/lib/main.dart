import 'package:flutter/material.dart';

import './TopBar.dart';

import './SizeManager.dart';

import './TextInput.dart';

import './SSO_Button.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {

    SizeManager sizeManager = new SizeManager(context);

    return Scaffold(
      body: Column(
          children: <Widget>[
            TopBar(),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(
                    top: sizeManager.scaledHeight(10),
                    left: sizeManager.scaledWidth(7),
                ),
                width: sizeManager.scaledWidth(86),
                height: sizeManager.scaledHeight(23),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Email', textAlign: TextAlign.left)
                    ),
                    TextInput(),
                    Container(
                      margin: EdgeInsets.only(top: sizeManager.scaledHeight(4)),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Password', textAlign: TextAlign.left)
                      ),
                    ),

                    TextInput(),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: sizeManager.scaledHeight(1)),
                        child: Text('Forgot your password?')
                      ),

                    ),



                  ],
                )
              ),
            ),

            Container(
              margin: EdgeInsets.only(top: sizeManager.scaledHeight(3)),
              width: sizeManager.scaledWidth(30),
              height: sizeManager.scaledHeight(6),
              child: InkWell(
                child: Container(
                    width: sizeManager.scaledWidth(100),
                    height: sizeManager.scaledHeight(100),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          const Radius.circular(50.0),
                        ),
                        color: Color(0xFF16b67f)
                    ),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Let\'s go!',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                    )
                ),

                onTap: () {
                  print("Logged in");
                },
              )
            ),

            SSO_Button(4,'assets/google.png','Google'),
            SSO_Button(0,'assets/apple.png', 'Apple'),
            SSO_Button(0, 'assets/office365.png', 'Office365'),
            
            Container(
              margin: EdgeInsets.only(top: sizeManager.scaledHeight(4)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Don\'t have an account?'),
                  Text(' Sign up!', style: TextStyle(fontWeight: FontWeight.bold))
                ],
              ),
            ),







          ],
        ),
    );
  }
}
