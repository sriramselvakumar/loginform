import 'package:flutter/material.dart';
import './SizeManager.dart';

class SSO_Button extends StatelessWidget{

  final double topMargin;
  final String imageName;
  final String SSO_method;

  SSO_Button(this.topMargin, this.imageName, this.SSO_method);



  Widget build(BuildContext context) {

    SizeManager sizeManager = new SizeManager(context);

    return Container(
          margin: EdgeInsets.only(
              top: sizeManager.scaledHeight(this.topMargin),
              left: sizeManager.scaledWidth(30)
          ),
          child: Row(
            children: <Widget>[
              Image(
                  height: sizeManager.scaledHeight(7),
                  width: sizeManager.scaledWidth(7),
                  image: AssetImage(this.imageName)
              ),
              Text('Sign in with '+this.SSO_method),

            ],
          )

      );
  }
}