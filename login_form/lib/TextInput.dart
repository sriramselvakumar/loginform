import 'package:flutter/material.dart';
import './SizeManager.dart';

class TextInput extends StatelessWidget{
  Widget build(BuildContext context) {
    SizeManager sizeManager = new SizeManager(context);

    return Align(
        alignment: Alignment.centerLeft,
        child: Container(
            width: sizeManager.scaledWidth(85),
            height: sizeManager.scaledHeight(4),
            margin: EdgeInsets.only(
                top: sizeManager.scaledHeight(1.5)
            ),

            child: TextField(
              decoration: InputDecoration(
                fillColor: Color(0xFFe8ecfc),
                filled: true,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF16b67f), width: 1.5),
                  borderRadius: BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
            )
        )
    );
  }
}